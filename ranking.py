from bs4 import BeautifulSoup
import requests
import json
import re
import time

url = ["https://www.lazada.com.ph/shop-traditional-laptops/?spm=a2o4l.searchlist.cate_1_3.1.2f8959caLflG1X",
"https://www.lazada.com.ph/shop-traditional-laptops/?page=2&spm=a2o4l.searchlist.cate_1_3.1.2f8959caLflG1X",
"https://www.lazada.com.ph/shop-traditional-laptops/?page=3&spm=a2o4l.searchlist.cate_1_3.1.2f8959caLflG1X"]

count = 0
result={"products":[]}
urlcount = 0
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'}


while True and (count < 100):
    r = requests.get(url[urlcount],headers=headers)
    raw_html = r.text
    soup = BeautifulSoup(raw_html,'html.parser')
   

    tag = soup.find("script",text=re.compile('"@type":"ItemList"'))
    if tag:
        data = tag.text.split('<script type="application/ld+json">')[0].split('</script>')[0]
        data2 = json.loads(data)
        for item in data2['itemListElement']:
            if (len(result['products'])<100):
                result['products'].append({
                    'image_url':item['image'],
                    'title':item['name'],
                    'url':item['url'],
                    'price':item['offers']['price']})
                count += 1
            else:
                break
        urlcount += 1
        print ('done') 
        time.sleep(120)
    else:
        print("Retrying URL: ",url[urlcount])
        time.sleep(120)
        continue

with open('result1.json', 'w') as outfile:
    json.dump(result, outfile)
    

    