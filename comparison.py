from bs4 import BeautifulSoup
import requests
import json
import re



url = "https://www.lazada.com.ph/products/lenovo-laptop-thinkpad-x230-intel-core-i5-4gb-ram-ddr3-320gb-7200rpm-hdd-intel-hd-4000-i519338233-s1391468510.html?spm=a2o4l.searchlist.list.41.2f8959caLflG1X&search=1"

r = requests.get(url)
raw_html = r.text
soup = BeautifulSoup(raw_html,'html.parser')


available = soup.find('script',type="application/ld+json")
ref1 = json.loads(available.text)



tag = soup.find("script",text=re.compile("window.LZD_RETCODE_PAGENAME = 'pdp-pc';"))
data = tag.text.strip().split('app.run(')[1].split(');')[0]
data2 = json.loads(data)

spec = data2['data']['root']['fields']['specifications']
sp = list(spec.keys())

delikeys = data2['data']['root']['fields']['deliveryOptions']
dk = list(delikeys.keys())

lstimg = []
lstvid = []
dcdelivery = {}
dcshipping = {}

for item in data2['data']['root']['fields']['deliveryOptions'][dk[0]]:
    for i in item:
        if 'fee' in item:
            dcdelivery[item['title']] = item['duringTime']
            dcshipping[item['title']] = item['feeValue']

for url in data2['data']['root']['fields']['skuGalleries']['0']:
    if url['type'] == 'img':
        lstimg.append(url['src'])
    elif url['type'] == 'video':
        lstvid.append(url['src'])




result={
    "price": {"value": ref1['offers']['price'],"currency": ref1['offers']['priceCurrency'] },
    "stocks": ref1['offers']['availability'],
    "rating": {
        "score": ref1['aggregateRating']['ratingValue'],
        "reviewers": ref1['aggregateRating']['ratingCount']},
    "product_title": data2['data']['root']['fields']['product']['title'],
    "product_description": ref1['description'],
    "product_videos": lstvid,
    "specifications": data2['data']['root']['fields']['specifications'][sp[0]]['features'],
    "img_url": lstimg,
    "delivery": dcdelivery,
    "shipping": dcshipping,

}

with open('result.json', 'w') as outfile:
    json.dump(result, outfile)




    

