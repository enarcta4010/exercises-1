from bs4 import BeautifulSoup
import requests
import re
import json
import urllib
from time import sleep
from random import randint


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'}


def setSoup(url):
    r = requests.get(url,proxies = urllib.request.getproxies(),headers=headers,timeout = 20)
    raw_html = r.text
    return BeautifulSoup(raw_html,'html.parser')

def comparison(inurl):
  
    # sleep(randint(10,15))
    soup1 = setSoup(inurl)
    tag1 = soup1.find("script",text=re.compile("window.LZD_RETCODE_PAGENAME = 'pdp-pc';"))
    data = tag1.text.strip().split('{"root":')[1].split(',"block-XLJkVvcRA":')[0]
    prod2 = json.loads(data)
    available = soup1.find('script',type="application/ld+json")
    ref1 = json.loads(available.text)
    if (tag1 and available):
        if ('aggregateRating') in ref1:
            comparison.score = ref1['aggregateRating']['ratingValue']
            comparison.reviewers = ref1['aggregateRating']['ratingCount']
            comparison.description = ref1['description']
        else:
            comparison.score = ""
            comparison.reviewers = ""
            comparison.description = ""
        comparison.stock = prod2['fields']['skuInfos']['0']['stock']
        comparison.brand = prod2['fields']['product']['brand']['name']
        for url in prod2['fields']['skuGalleries']['0']:
            if url['type'] == 'img':
                lstimg.append(url['src'])
    else:
        comparison(inurl)



def increurl(url,countp):
    if(countp > 1):
        urlfirst = url.split('?')[0]
        urllast = url.split(urlfirst)[1].split('?')[1]
        increurlf = url.split('?')[0]+"?page="+str(countp)+"&"+url.split(url.split('?')[0])[1].split('?')[1]
        print (increurlf)
        return increurlf
        
    else:
        print (url)
        return url
       

    





result = {'products':[]}    
itemCount = 0
page1 = 1
# url1 ="https://www.lazada.com.ph/shop-traditional-laptops/?spm=a2o4l.searchlist.cate_1_3.1.2f8959caLflG1X"
url1="https://www.lazada.com.ph/shop-moto-cleaners/?spm=a2o4l.home.cate_12_8.1.3a656db4ecfAOc"
while True:
    soup = setSoup(increurl(url1,page1))
    

    if soup:
        tag = soup.find("script",text = re.compile('"@type":"ItemList"'))
        page = soup.find("script",text = re.compile('window.pageData'))
        data = tag.text.split('<script type="application/ld+json">')[0].split('</script>')[0]
        items = json.loads(data)
        pg = (page.text.split('window.pageData=')[1].split('</script>')[0])
        pg2 = json.loads(pg)
        currpage = pg2['mainInfo']['page']
        totalItems = pg2['mainInfo']['dataLayer']['page']['resultNr']+1
        print(str(totalItems)+' total items')
        lstimg = []
        
        

    
        if (items):
            for item in items['itemListElement']:
                
                value = item['offers']['price']
                currency = item['offers']['priceCurrency']
                title = item['name']
                url = item['url']
                rank = itemCount+1
                rating={}
                comparison(url)
                description = comparison.description
                stock = comparison.stock
                brand = comparison.brand
                result['products'].append({
                    'price':{
                        'value':value,
                        'currency':currency
                    },
                    'url':url,
                    'title':title,
                    'description':description,
                    'rating':{
                        'score':comparison.score,
                        'reviewers':comparison.reviewers
                    },
                    'brand':brand,
                    'stock':stock,
                    'img_url':lstimg,
                    'page_number':currpage,
                    'rank':str(rank),


                })
                itemCount += 1
                print (itemCount)
                with open('result2.json', 'w') as outfile:
                    json.dump(result, outfile)
                lstimg.clear()
            page1 += 1
            
        else:
            break













